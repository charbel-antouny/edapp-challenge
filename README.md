# EDAPP FULL STACK CODING CHALLENGE

Refer to PDF document for challenge details.

## Dependencies

- React 16.0
- .NET core 3.1

## How to run

1. Open the solution in Visual Studio.
2. The default configuration should start the project in dev mode and open the browser with the website.
