using Microsoft.VisualStudio.TestTools.UnitTesting;
using auction_website;

namespace auction_website_tests {
    [TestClass]
    public class AuctionWebsiteTests {
        [TestMethod]
        public void IsValidBid() {
            // Arrange
            // Receive a new bid from a user

            // Act
            // Attempt to update current bid

            // Assert
            // The bid should be greater than the current bid
        }

        [TestMethod]
        public void ItemNotExpired() {
            // Arrange
            // Receive a new bid from a user

            // Act
            // Attempt to update current bid

            // Assert
            // The item auction window should still be active (not expired)
        }

        [TestMethod]
        public void IsWatchingItem() {
            // Arrange
            // User bids on an item

            // Act
            // The bid is updated

            // Assert
            // The item should be added to the user's watch list
        }

        [TestMethod]
        public void CreateAccount() {
            // Arrange
            // User creates an account

            // Act
            // Account is created and added to database

            // Assert
            // The account is active and the user can login
        }

        [TestMethod]
        public void UpdateBid() {
            // Arrange
            // Receive a new bid from a user

            // Act
            // Attempt to update current bid

            // Assert
            // The bid and current winner are updated
        }
    }
}
