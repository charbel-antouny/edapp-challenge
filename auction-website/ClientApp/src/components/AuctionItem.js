﻿import React, { Component } from 'react';
//import { event } from 'jquery';

class AuctionItem extends Component {
    static displayName = AuctionItem.name;

    //constructor() {
    //    super();
    //    this.handleSubmit = this.handleSubmit.bind(this);
    //}

    state = { auctionItems: [], loading: true };

    componentDidMount() {
        this.getAuctionItemData();
    }

    static renderAuctionItemsTable = (auctionItems) => {
        return (
            <div>
                <table className='table table-striped' aria-labelledby="tabelLabel">
                    <thead>
                        <tr>
                            <th>Item Id</th>
                            <th>Image</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Expires</th>
                            <th>Current Bid</th>
                            <th>Current Winner</th>
                        </tr>
                    </thead>
                    <tbody>
                        {auctionItems.map(item =>
                            <tr key={item.id}>
                                <td>{item.id}</td>
                                <td>{item.imageUrl}</td>
                                <td>{item.title}</td>
                                <td>{item.description}</td>
                                <td>{item.endDate}</td>
                                <td>{item.currentBid}</td>
                                <td>{item.currentWinner}</td>
                            </tr>
                        )}
                    </tbody>
                </table>
                <form onSubmit={AuctionItem.handleSubmit}>
                    <label htmlFor="productId">Product Id</label>
                    <input id="productId" name="productId" type="text" />

                    <label htmlFor="username">Username</label>
                    <input id="username" name="username" type="text" />

                    <label htmlFor="bid">Enter your bid ($)</label>
                    <input id="bid" name="bid" type="text" />

                    <button>Submit</button>
                </form>
            </div>
        );
    };

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : AuctionItem.renderAuctionItemsTable(this.state.auctionItems);

        return (
            <div>
                <h1 id="tabelLabel" >Auction Items</h1>
                <p>Place a bid on an item!</p>
                {contents}
            </div>
        );
    }

    getAuctionItemData = async () => {
        const response = await fetch('auctionitems');
        const data = await response.json();
        this.setState({ auctionItems: data, loading: false });
    }

    static handleSubmit(event) {
        console.log(event);
        event.preventDefault();
        const data = new FormData(event.target);

        const response = fetch('auctionitems', {
            method: 'POST',
            body: data,
        });
        console.log(JSON.stringify(response));
    }
}

export default AuctionItem;