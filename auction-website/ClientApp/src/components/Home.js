import React, { Component } from 'react';

export class Home extends Component {
  static displayName = Home.name;

  render () {
    return (
      <div>
        <h1>Welcome to eBae</h1>
        <p>Use the navigation bar to browse the website.</p>
        <p>Happy bidding!</p>
        </div>
    );
  }
}
