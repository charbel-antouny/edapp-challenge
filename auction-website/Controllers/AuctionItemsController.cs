﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using auction_website;
using Microsoft.AspNetCore.Mvc;

namespace auction_website.Controllers {

    [ApiController]
    [Route("[controller]")]
    public class AuctionItemsController : ControllerBase {
        public AuctionItemsController() {
            Items = new AuctionItem[] {
                new AuctionItem {
                    Id = "1",
                    Title = "Banana",
                    Description = "A scrumptious banana, full of potassium!",
                    CurrentBid = 0,
                    CurrentWinner = "No bids yet",
                    EndDate = DateTime.Now.AddDays(3).ToString("g"),
                    ImageUrl = "Image goes here",
                },
                new AuctionItem {
                    Id = "2",
                    Title = "Apple",
                    Description = "A delicious apple to keep the doctor away!",
                    CurrentBid = 0,
                    CurrentWinner = "No bids yet",
                    EndDate = DateTime.Now.AddDays(7).ToString("g"),
                    ImageUrl = "Image goes here",
                },
                new AuctionItem {
                    Id = "3",
                    Title = "Mango",
                    Description = "A juicy summer treat!",
                    CurrentBid = 0,
                    CurrentWinner = "No bids yet",
                    EndDate = DateTime.Now.AddDays(12).ToString("g"),
                    ImageUrl = "Image goes here",
                },
            };
        }


        private AuctionItem[] Items;

        [HttpPost]
        //[Route("[controller]/bid")]
        public void Post([FromBody] System.Net.Http.MultipartFormDataContent data) {
            System.Diagnostics.Debug.WriteLine(data);
            // Update Items
            // return success or fail
        }

        [HttpGet]
        public IEnumerable<AuctionItem> Get() {
            return Items;
        }
    }
}
