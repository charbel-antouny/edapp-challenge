﻿using System;

namespace auction_website {
    public class AuctionItem {
        public string Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string EndDate { get; set; }

        public double CurrentBid { get; set; }

        public string CurrentWinner { get; set; }

        public string ImageUrl { get; set; }
    }
}
